// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkateBoard.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SkateBoard, "SkateBoard" );
 