// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkateBoardGameMode.generated.h"

UCLASS(minimalapi)
class ASkateBoardGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASkateBoardGameMode();
};



